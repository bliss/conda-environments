Conda environments
------------------

A repository to store conda environments.

Usage:

``` console
$ conda env create -f mxcube2-dev.yml
```
